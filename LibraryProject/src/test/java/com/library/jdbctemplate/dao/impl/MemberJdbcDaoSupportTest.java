package com.library.jdbctemplate.dao.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.library.domain.Book;
import com.library.domain.Member;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:autoWireConfiguration.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })


public class MemberJdbcDaoSupportTest {

	@Autowired
	ApplicationContext autoWireContext;
	MemberJdbcDaoSupport memberJdbcDaoSupport;
	
	@Before
	public void setUp() throws Exception {
		memberJdbcDaoSupport=(MemberJdbcDaoSupport)autoWireContext.getBean("memberJdbcDaoSupport");
	
	}
	
	//Test Create Member
	@Test
	@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
	public void testCreateMember() {
		memberJdbcDaoSupport=(MemberJdbcDaoSupport)autoWireContext.getBean("memberJdbcDaoSupport");
		memberJdbcDaoSupport.createMember("John Murphy","CIT","Bishopstown","Cork","086-123434");
		int rowCount=memberJdbcDaoSupport.countRows();
		assertEquals(2, rowCount);	
	
	}
	
	// Test Delete Member WITH id=1
	@Test
	@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberInteger(){
		int id = 1;
		memberJdbcDaoSupport.deleteMember(id);
	    int rowCount=memberJdbcDaoSupport.countRows();
	    assertEquals(0, rowCount);
	}


	
	// Test update member address given the id
	@Test
	@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
	public void testUpdateMember() {
		MemberJdbcDaoSupport memberJdbcDaoSupport=(MemberJdbcDaoSupport)autoWireContext.getBean("memberJdbcDaoSupport");
		memberJdbcDaoSupport.updateMember(1, "TheGrove", "Wilton", "Cork");
		List<Member> members=memberJdbcDaoSupport.listMembers();
		int rowCount=memberJdbcDaoSupport.countRows();
		assertEquals(1, rowCount);
		assertEquals(members.get(0).getAddress1(),"TheGrove");
		System.out.println("Updated Record of member with id=1 : ");

	
	}
	
	// Test Delete Member with first name and last name
	@Test
	@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberByName(){
		String firstname = "John";
		String lastname = "Murphy";
		memberJdbcDaoSupport.deleteMember(firstname, lastname);
		int rowCount=memberJdbcDaoSupport.countRows();
		assertEquals(0, rowCount);
	
	}
	
	// Test List members by create and record
	@Test
   	@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
	public void testListBooks() {
		memberJdbcDaoSupport.createMember("John Smyth","TestAddress1","TestAddress2","TestTown","Test_Number");
		List<Member> members=memberJdbcDaoSupport.listMembers();		
		assertEquals(members.size(), 2);
		assertEquals(members.get(1).getName(),"John Smyth");
		assertEquals(members.get(0).getName(),"John Murphy");
		System.out.print("Record added to list of members : ");
		System.out.println(members.get(1).getName());
			
	} 
	
				
}
