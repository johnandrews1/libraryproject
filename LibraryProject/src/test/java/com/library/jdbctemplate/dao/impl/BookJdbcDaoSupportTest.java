package com.library.jdbctemplate.dao.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.library.domain.Book;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:autoWireConfiguration.xml"})
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })

public class BookJdbcDaoSupportTest {

	@Autowired
	ApplicationContext autoWireContext;
	BookJdbcDaoSupport bookJdbcDaoSupport;
		
	@Before
	public void setUp() throws Exception {
		bookJdbcDaoSupport=(BookJdbcDaoSupport)autoWireContext.getBean("bookJdbcDaoSupport");
	
	}
	
		//Test create a book 
		@Test
		@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
		public void testCreateBook() {
		Date pub_date = new Date();
		BookJdbcDaoSupport bookJdbcDaoSupport=(BookJdbcDaoSupport)autoWireContext.getBean("bookJdbcDaoSupport");
		bookJdbcDaoSupport.createBook("famous 5", "enid blyton", "fallons", pub_date, "TestIsbn");
		int rowCount=bookJdbcDaoSupport.countRows();
		assertEquals(2, rowCount);
	
		}	
		
		// Test Delete a book WITH id=1
		@Test
		@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
		public void testDeleteBookInteger(){
			int id = 1;
			bookJdbcDaoSupport.deleteBook(id);
			int rowCount=bookJdbcDaoSupport.countRows();
			assertEquals(0, rowCount);
	    }
	
		//Test Delete a book by Title
		@Test
		@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
		public void testDeleteBookString() {
			String my_title = "50 Shades of Grey";
			bookJdbcDaoSupport.deleteBook(my_title);
			int rowCount=bookJdbcDaoSupport.countRows();
			assertEquals(0, rowCount);
		}
		
		// Test List books
		@Test
		@DatabaseSetup(value="classpath:databaseEntries.xml", type=DatabaseOperation.CLEAN_INSERT)
		public void testListBooks() {
			Date pub_date = new Date();
			bookJdbcDaoSupport.createBook("The Hobbit","","Tolkien", pub_date, "Isbn");
			List<Book> books=bookJdbcDaoSupport.listBooks();		
			assertEquals(books.size(), 2);	
			assertEquals(books.get(0).getTitle(),"50 shades of grey");
			assertEquals(books.get(1).getTitle(),"The Hobbit");
		} 
}
