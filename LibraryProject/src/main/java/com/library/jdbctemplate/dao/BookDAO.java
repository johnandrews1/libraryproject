package com.library.jdbctemplate.dao;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import com.library.domain.Book;

public interface BookDAO {
	public void setDataSource(DataSource ds);
	
	public void createBook();
	
	public void createBook(String title, String author, String publisher, 
			Date publication_date, String isbn);	

	public void deleteBook(Integer id);
	
	public void deleteBook(String title);
	
	public List<Book> listBooks();
	
	public List<Book> listBooks(Integer id);
	
	public List<Book> listBooks(Integer member_id,Integer id);

	public int countRows();

		
}

