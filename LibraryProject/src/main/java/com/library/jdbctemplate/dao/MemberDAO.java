package com.library.jdbctemplate.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.transaction.TransactionStatus;

import com.library.domain.Member;

public interface MemberDAO {
	
	public void setDataSource(DataSource ds);
    
	public void createMember(String name, String address1, String address2, String town, String contact_number);
	
	public void deleteMember(Integer id);
	
	public void deleteMember(String firstname, String Lastname);
	
	public void updateMember(Integer id);
	
	public void updateMember(Integer id, String address1, String address2, String town); 
	
	//public void deleteMember(String name);
	
	public List<Member> listMembers();
	
	public List<Member> listMembers(Integer id);
	
	public List<Member> listMembers(String title);
	
	public List<Member> listMembers(String title1, String title2, String title3);
	
	public int countRows();
	
	
}
