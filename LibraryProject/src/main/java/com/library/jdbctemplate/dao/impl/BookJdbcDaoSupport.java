package com.library.jdbctemplate.dao.impl;


import java.sql.Types;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import com.library.domain.Book;
import com.library.jdbctemplate.dao.BookDAO;


@Repository
public class BookJdbcDaoSupport extends JdbcDaoSupport implements BookDAO{
	
	@Autowired
	BookJdbcDaoSupport(DataSource dataSource) {
		   setDataSource(dataSource);
	}
	
	@Override
	public void createBook() {
	}
	
	@Override
	public void createBook(String title, String author, String publisher, 
			Date publication_date, String isbn) {
		
		
		String SQL = "insert into Book (title, author, publisher, publication_date, isbn) values (?, ?, ?, ?, ?)";
		
		Object[] params=new Object[]{ title, author, publisher, 
				publication_date, isbn};
		PreparedStatementCreatorFactory psc=new PreparedStatementCreatorFactory(SQL);
		psc.addParameter(new SqlParameter("title", Types.VARCHAR));
		psc.addParameter(new SqlParameter("author", Types.VARCHAR));
		psc.addParameter(new SqlParameter("publisher", Types.VARCHAR));
		psc.addParameter(new SqlParameter("publication_date", Types.DATE));
		psc.addParameter(new SqlParameter("isbn", Types.VARCHAR));
	    
		KeyHolder holder = new GeneratedKeyHolder();
		this.getJdbcTemplate().update(psc.newPreparedStatementCreator(params), holder);		
		System.out.println("Created Record for Book = " + title);
		return;		
	}

	@Override
	public int countRows() {
		String SQL = "select count(*) from book";
		int rows=getJdbcTemplate().queryForObject(SQL, Integer.class);
		return rows;
	}
	
	@Override
	public void deleteBook(Integer id) {
		String SQL = "delete from Book where id = ?";
		getJdbcTemplate().update(SQL, new Object[] {id});
		System.out.println("Deleted Record with ID = " + id );
		return;		
	}   

	@Override
	public void deleteBook(String title) {
		String SQL = "delete from Book where title = ?";
		getJdbcTemplate().update(SQL, new Object[] {title});
		System.out.println("Deleted Record with title = " + title );
		return;		
	}   

	@Override
	public List<Book> listBooks() {
		String SQL = "select * from book";
		List<Book> bookList = getJdbcTemplate().query(SQL, 
						new BookMapper());
		return bookList;
	}
	
   
	@Override
	public List<Book> listBooks(Integer id) {
	
		return null;
	}

	@Override
	public List<Book> listBooks(Integer member_id, Integer id) {

		return null;
	}

}
