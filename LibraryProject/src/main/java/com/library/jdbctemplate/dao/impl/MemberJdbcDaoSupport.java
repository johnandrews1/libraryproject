package com.library.jdbctemplate.dao.impl;

import java.sql.Types;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;

import com.library.domain.Book;
import com.library.domain.Member;
import com.library.jdbctemplate.dao.MemberDAO;



@Repository
public class MemberJdbcDaoSupport extends JdbcDaoSupport implements MemberDAO {

	@Autowired
	MemberJdbcDaoSupport(DataSource dataSource) {
		   setDataSource(dataSource);
	}
	
	private JdbcTemplate jdbcTemplateObject;
	@Override
	public void createMember(String name, String address1, String address2, String town, String contact_number) {

		String SQL = "insert into member (name, address1, address2, town, contact_number) values (?, ?, ?, ?, ?)";
		
		Object[] params=new Object[]{ name, address1, address2, town, contact_number};
		PreparedStatementCreatorFactory psc=new PreparedStatementCreatorFactory(SQL);
		psc.addParameter(new SqlParameter("name", Types.VARCHAR));
		psc.addParameter(new SqlParameter("address1", Types.VARCHAR));
		psc.addParameter(new SqlParameter("address2", Types.VARCHAR));
		psc.addParameter(new SqlParameter("town", Types.VARCHAR));
		psc.addParameter(new SqlParameter("contact_number", Types.VARCHAR));
			
		KeyHolder holder = new GeneratedKeyHolder();
		this.getJdbcTemplate().update(psc.newPreparedStatementCreator(params), holder);		
		System.out.println("Created Record Name = " + name + " Contact Number = " + contact_number);
		return;	
		
	}

	@Override
	public void deleteMember(Integer id) {
		String SQL = "delete from Member where id = ?";
		getJdbcTemplate().update(SQL, new Object[] {id});
		System.out.println("Deleted Record with ID = " + id );
		return;		
	}   

	@Override
	public void deleteMember(String firstname, String lastname) {
		String full_name=(firstname + " " + lastname);
		String SQL = "delete from Member where name = ?";
		getJdbcTemplate().update(SQL, new Object[] {full_name});
		System.out.println("Deleted Record with name = " + full_name );
		return;		
	} 
	

	@Override
	public void updateMember(Integer id, String address1, String address2, String town) {
	
		String SQL = "update member set address1 = ? where id = ?";
		getJdbcTemplate().update(SQL,  new Object[] {address1, id});
		String SQL1 = "update member set address2 = ? where id = ?";
		getJdbcTemplate().update(SQL1,  new Object[] {address2, id});
		String SQL2 = "update member set town = ? where id = ?";
		getJdbcTemplate().update(SQL2,  new Object[] {town, id});
			
		System.out.println("Updated Record with ID = " + id );
		return;
	}
	
	@Override
	public void updateMember(Integer id) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<Member> listMembers() {
		String SQL = "select * from member";
		List<Member> memberList = getJdbcTemplate().query(SQL, 
					new MemberMapper());
		return memberList;
	}

	@Override
	public List<Member> listMembers(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Member> listMembers(String title) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Member> listMembers(String title1, String title2, String title3) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int countRows() {
		String SQL = "select count(*) from member";
		//int rows=jdbcTemplate.queryForObject(SQL, Integer.class);
		int rows=getJdbcTemplate().queryForObject(SQL, Integer.class);
		return rows;
	}
	
	
}
	

