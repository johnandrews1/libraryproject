package com.library.jdbctemplate.dao;

import java.util.List;

import com.library.domain.Member_loans_Book;

public interface Member_loans_BookDAO {

	public List<Member_loans_Book> listMember_loans_books();
	
}
