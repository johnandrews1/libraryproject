package com.library.jdbctemplate.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.library.domain.Member;

public class MemberMapper  implements RowMapper{

	 public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
	 
	      Member member = new Member();
	      member.setId(rs.getInt("id"));
	      member.setName(rs.getString("name"));
          member.setAddress1(rs.getString("address1"));
	      member.setAddress2(rs.getString("address2"));
	      member.setTown(rs.getString("town"));
	      member.setContact_number(rs.getString("contact_number"));
	      member.setBook_allowance(rs.getInt("book_allowance"));
	      member.setBalance(rs.getDouble("balance"));
	      member.setActive(rs.getBoolean("active"));
	      return member;
	   }
}       