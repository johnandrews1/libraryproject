package com.library.jdbctemplate.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.library.domain.Book;

public class BookMapper implements RowMapper {
	
	 public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		 Book book = new Book();
		 book.setId(rs.getInt("id"));
		 book.setTitle(rs.getString("title"));
		 book.setAuthor(rs.getString("author"));
		 book.setPublisher(rs.getString("publisher"));
		 book.setPublication_date(rs.getDate("publication_date"));
		 book.setIsbn(rs.getString("isbn"));
		 book.setAvailable(rs.getBoolean("available"));
	     return book;
	
	 }

}