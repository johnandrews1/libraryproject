package com.library.domain;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

public class Member_loans_Book {

	private Integer member_id;
	private Integer book_id;
	private Date loan_date;
	private Date return_date;
	private Double fine;

	
	@Autowired
	public Member_loans_Book(Integer member_id, Integer book_id,
			Date loan_date, Date return_date, Double fine) {
			
		this.member_id = member_id;
		this.book_id = book_id;
		this.loan_date = new Date();
		this.return_date = return_date;
		this.fine = fine;
	
	}
	
	public Member_loans_Book() {
	}
	
	public Integer getMember_id() {
		return member_id;
	}

	public void setMember_id(Integer member_id) {
		this.member_id = member_id;
	}

	public Integer getBook_id() {
		return book_id;
	}

	public void setBook_id(Integer book_id) {
		this.book_id = book_id;
	}
	public Date getLoan_date() {
		return loan_date;
	}

	public void setLoan_date(Date loan_date) {
		this.loan_date = loan_date;
	}

	public Date getReturn_date() {
		return return_date;
	}

	public void setReturn_date(Date return_date) {
		this.return_date = return_date;
	}

	public Double getFine() {
		return fine;
	}

	public void setFine(Double fine) {
		this.fine = fine;
	}

}
