package com.library.domain;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Book {
	private Integer id;
	private String title;
	private String author;
	private String publisher;
	private Date publication_date;
	private String isbn;
	private boolean available;
	

	@Autowired
	public Book(@Value("${title}") String title, @Value("${author}") String author,
			 @Value("${publisher}") String publisher, @Value("${publication_date}")Date publication_date,
			@Value("${isbn}") String isbn, @Value("true")boolean available){

		this.title=title;
		this.author=author;
		this.publisher=publisher;
		this.publication_date=publication_date;
		this.isbn=isbn;
		this.available=available;

	}
	public Book(){
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public Date getPublication_date() {
		return publication_date;
	}
	public void setPublication_date(Date publication_date) {
		this.publication_date = publication_date;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public boolean getAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
}
	
