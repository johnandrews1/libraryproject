package com.library.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class Member {
	
	private Integer id;
	private String name;
	private String address1;
	private String address2;
	private String town;
	private String contact_number;
	private Integer book_allowance;
	private Double balance;
	private boolean active;
	
	@Autowired
	public Member(@Value("${name}") String name, @Value("${address1}") String address1,
			@Value("${address2}") String address2, @Value("${town}") String town, @Value("${contact_number}") String contact_number,
			@Value("${book_allowance}") Integer book_allowance, @Value("${balance}") Double balance,
			@Value("${active}")boolean active) {
	
	    this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.contact_number = contact_number;
		this.book_allowance = book_allowance;
		this.balance = balance;
		this.active = active;
	}
	public Member() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public Integer getBook_allowance() {
		return book_allowance;
	}
	public void setBook_allowance(Integer book_allowance) {
		this.book_allowance = book_allowance;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
}
