package com.library;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.library.jdbctemplate.dao.impl.BookJdbcDaoSupport;
import com.library.jdbctemplate.dao.impl.MemberJdbcDaoSupport;

/**
 * Hello world!
 *
 */
public class App 
{
    private static ApplicationContext autoWirecontext;
    
    public static void main( String[] args )
    {
    	
        autoWirecontext=new ClassPathXmlApplicationContext
                ("autoWireConfiguration.xml");
    
}
}
